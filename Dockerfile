FROM ubuntu
RUN apt update && apt install -y curl && rm -rf /var/lib/apt/lists/*
RUN VERSION=$(curl --silent "https://api.github.com/repos/goodwithtech/dockle/releases/latest" | grep '"tag_name":' | sed -E 's/.*"v([^"]+)".*/\1/') && curl -L -o dockle.deb https://github.com/goodwithtech/dockle/releases/download/v${VERSION}/dockle_${VERSION}_Linux-64bit.deb && dpkg -i dockle.deb && rm dockle.deb
RUN curl -L -o trivy.deb https://github.com/aquasecurity/trivy/releases/download/v0.21.2/trivy_0.21.2_Linux-64bit.deb && dpkg -i trivy.deb && rm trivy.deb
RUN mkdir /workspace
WORKDIR /workspace